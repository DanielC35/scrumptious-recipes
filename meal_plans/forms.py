from django.forms import ModelForm

from meal_plans.models import Mealplan


class MealplanForm(ModelForm):
    class Meta:
        model = Mealplan
        fields = ["name", "date", "owner", "recipes"]


class MealplanDeleteForm(ModelForm):
    class Meta:
        model = Mealplan
        fields = ["name", "date", "owner", "recipes"]
