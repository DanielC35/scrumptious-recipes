from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from meal_plans.models import Mealplan
from django.contrib.auth.mixins import LoginRequiredMixin


# Create your views here.
class MealplanListView(LoginRequiredMixin, ListView):
    model = Mealplan
    context_object_name = "mealplans"
    template_name = "meal_plans/list.html"
    paginate_by = 20

    def get_queryset(self):
        return Mealplan.objects.filter(owner=self.request.user)

    def get_querylist(self):
        querystring = self.request.GET.get("q")
        if querystring is None:
            querystring = ""
        return Mealplan.objects.filter(description__icontains=querystring)


class MealplanDetailView(LoginRequiredMixin, DetailView):
    model = Mealplan
    context_object_name = "mealplan"
    template_name = "meal_plans/detail.html"

    def get_queryset(self):
        return Mealplan.objects.filter(owner=self.request.user)


class MealplanCreateView(LoginRequiredMixin, CreateView):
    model = Mealplan
    template_name = "meal_plans/new.html"
    fields = ["name", "date", "recipes"]
    success_url = "/"

    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.owner = self.request.user
        plan.save()
        form.save_m2m()
        return redirect("meal_plans_detail", pk=plan.id)


class MealplanDeleteView(LoginRequiredMixin, DeleteView):
    template_name = "meal_plans/delete.html"
    success_url = "/"
    context_object_name = "meal_plans_delete"

    def get_queryset(self):
        return Mealplan.objects.filter(owner=self.request.user)


class MealplanUpdateView(LoginRequiredMixin, UpdateView):
    model = Mealplan
    template_name = "meal_plans/edit.html"
    fields = ["name", "date", "owner"]
    success_url = "/"
    context_object_name = "meal_plans_edit"

    def get_queryset(self):
        return Mealplan.objects.filter(owner=self.request.user)

    def get_success_url(self) -> str:
        return reverse_lazy("meal_plan_detail", args=[self.object.id])
