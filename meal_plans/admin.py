from django.contrib import admin
from meal_plans.models import Mealplan


# Register your models here.
class MealplanAdmin(admin.ModelAdmin):
    pass


admin.site.register(Mealplan, MealplanAdmin)