from django.urls import path

from recipes.views import (
    RecipeDeleteView,
    RecipeDetailView,
    RecipeUpdateView,
    log_rating,
    RecipeListView,
    RecipeCreateView,
    ShoppingListDeleteView,
    ShoppingListListView,
    ShoppingListCreateView,
    )

urlpatterns = [
    path("", RecipeListView.as_view(), name="recipes_list"),
    path("<int:pk>/edit/", RecipeUpdateView.as_view(), name="recipe_edit"),
    path("<int:pk>/", RecipeDetailView.as_view(), name="recipe_detail"),
    path("new/", RecipeCreateView.as_view(), name="recipe_new"),
    path("<int:recipe_id>/ratings/", log_rating, name="recipe_rating"),
    path("<int:pk>/delete/", RecipeDeleteView.as_view(), name="recipe_delete"),
    path("<int:pk>/delete/", ShoppingListDeleteView.as_view(), name="shoppinglists_delete"),
    path("", ShoppingListListView.as_view(), name="shoppinglists_list"),
    path("new/", ShoppingListCreateView.as_view(), name="shoppinglists_new"),
]
