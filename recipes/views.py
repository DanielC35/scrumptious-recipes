from django.shortcuts import redirect
from recipes.forms import RatingForm
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from recipes.models import Recipe, ShoppingList
from django.contrib.auth.mixins import LoginRequiredMixin


def log_rating(request, recipe_id):
    if request.method == "POST":
        form = RatingForm(request.POST)
        try:
            if form.is_valid():
                rating = form.save(commit=False)
                rating.recipe = Recipe.objects.get(pk=recipe_id)
                rating.save()
        except Recipe.DoesNotExist:
            return redirect("recipes_list")
    return redirect("recipe_detail", pk=recipe_id)


class RecipeListView(ListView):
    model = Recipe
    context_object_name = "recipes"
    template_name = "recipes/list.html"
    paginate_by = 20

    def get_queryset(self):
        querystring = self.request.GET.get("q")
        if querystring is None:
            querystring = ""
        return Recipe.objects.filter(description__icontains=querystring)


class RecipeDetailView(DetailView):
    model = Recipe
    context_object_name = "recipe"
    template_name = "recipes/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["rating_form"] = RatingForm()
        return context


class RecipeUpdateView(UpdateView):
    model = Recipe
    template_name = "recipes/edit.html"
    fields = ["name", "author", "description", "image"]
    success_url = "/"


class RecipeCreateView(LoginRequiredMixin, CreateView):
    model = Recipe
    template_name = "recipes/new.html"
    fields = ["name", "description", "image"]
    success_url = "/"

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class RecipeDeleteView(DeleteView):
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = "/"


class ShoppingListCreateView(CreateView):
    model = ShoppingList
    template_name = "shoppinglists/new.htmls"
    success_url = "/"


class ShoppingListListView(ListView):
    model = ShoppingList
    template_name = "shoppinglists/list.html"
    success_url = "/"


class ShoppingListDeleteView(DeleteView):
    model = ShoppingList
    template_name = "shoppinglists/list.html"
    success_url = "/"
